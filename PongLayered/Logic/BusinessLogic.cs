﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;

namespace Logic
{
    public class BusinessLogic : ILogic
    {
        public IGameModel Model { get; }

        

        public event EventHandler RefreshScreen;

        public BusinessLogic()
        {
            Model = new GameModel();
            SetUpEnemies();
            
        }


        public void MovePad(Direction d)
        {
            if (d == Direction.Left)
            {
                Model.Pad.ChangeX(-10);
            }
            else
            {
                Model.Pad.ChangeX(10);
            }
            RefreshScreen?.Invoke(this, EventArgs.Empty);
        }
        public void JumpPad(double x)
        {
            Model.Pad.SetXY(x, Model.Pad.Area.Y);
            RefreshScreen?.Invoke(this, EventArgs.Empty);
        }
        private bool MoveShape(Shape shape)
        {

            bool isFaulted = false;
            if (shape is Enemy)
            {
                shape.MoveRandom();

                if (shape.Area.Left < 0 || shape.Area.Right > Config.Width)
                {
                    shape.Dx = -shape.Dx;
                }
                if (shape.Area.Top < 0 || shape.Area.IntersectsWith(Model.Pad.Area))
                {
                    shape.Dy = -shape.Dy;
                    
                }
                if (shape.Area.Bottom > Config.Height)
                {
                    shape.Dy = -shape.Dy;
                }
                if (shape.Area.IntersectsWith(Model.Ball.Area))
                {
                    RemoveEnemy(shape as Enemy);
                    Model.Ball.MoveRandom();
                    Model.Points++;
                    RefreshScreen?.Invoke(this, EventArgs.Empty);

                    AddEnemy();
                }
            }
            else
            {
                shape.ChangeX(shape.Dx);
                shape.ChangeY(shape.Dy);
                if (shape.Area.Left < 0 || shape.Area.Right > Config.Width)
                {
                    shape.Dx = -shape.Dx;
                }
                if (shape.Area.Top < 0 || shape.Area.IntersectsWith(Model.Pad.Area))
                {
                    shape.Dy = -shape.Dy;
                }
                if (shape.Area.Bottom > Config.Height)
                {
                    isFaulted = true;
                    shape.SetXY(shape.Area.X, Config.Height / 2);
                }
            }


            return isFaulted;
        }
        public void MoveBall()
        {
            if (MoveShape(Model.Ball))
            {
                Model.Errors++;
            }
            RefreshScreen?.Invoke(this, EventArgs.Empty);

        }
        public void MoveEnemies()
        {

            foreach (var enemy in Model.Enemies.ToList()) // a .ToList miatt gyakorlatilag egy másolaton megy végig a foreach, így nincs hiba ha közben meghal egy ellenség (-> módosul a lista, emiatt alapból elszállna a program)
            {
                MoveShape(enemy);
            }
            RefreshScreen?.Invoke(this, EventArgs.Empty);
        }
        public void AddEnemy()
        {
            Model.Enemies.Add(new Enemy(Config.rnd.Next(100, (int)Config.Width), 20, Config.EnemySize, Config.EnemySize));
            RefreshScreen?.Invoke(this, EventArgs.Empty);
        }
        public void AddStar()
        {
            Model.Stars.Add(new Star(Config.Width / 2, Config.Height / 2, 10, 8));
            RefreshScreen?.Invoke(this, EventArgs.Empty);
        }
        public void MoveStars()
        {
            foreach (var star in Model.Stars)
            {
                if (MoveShape(star))
                {
                    Model.Errors++;
                }
            }
            RefreshScreen?.Invoke(this, EventArgs.Empty);
        }

        private void SetUpEnemies()
        {
            for (int i = 0; i < Config.CountOfEnemies; i++)
            {
                AddEnemy();
            }
        }
        private void RemoveEnemy(Enemy e)
        {
            Model.Enemies.Remove(e);
        }
    }
}
