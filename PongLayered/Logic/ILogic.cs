﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
namespace Logic
{
    public interface ILogic
    {
        event EventHandler RefreshScreen;
        IGameModel Model { get; }
        void MovePad(Direction d);
        void JumpPad(double x);
        void MoveBall();
        void MoveEnemies();
        void AddEnemy();
        void AddStar();
        void MoveStars();
    }
}
