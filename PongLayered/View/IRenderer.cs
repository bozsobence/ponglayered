﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using Model;

namespace View
{
    public interface IRenderer
    {
        IGameModel Model { get; }
        void DrawThings(DrawingContext ctx);
    }
}
