﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Model;
using Logic;

namespace View
{
    class Renderer
    {
        public ILogic Logic { get; }
        public Renderer(ILogic logic)
        {
            this.Logic = logic;
        }

        // 1. builder design pattern
        public void DrawThings(DrawingContext ctx)
        {
            DrawingGroup dg = new DrawingGroup();
            GeometryDrawing background = new GeometryDrawing(Config.BackgroundBrush, new Pen(Config.BorderBrush, Config.BorderSize), new RectangleGeometry(new Rect(0, 0, Config.Width, Config.Height)));
            GeometryDrawing ball = new GeometryDrawing(Config.BallBgBrush, new Pen(Config.BallBgBrush, 1), new EllipseGeometry(Logic.Model.Ball.Area));
            GeometryDrawing pad = new GeometryDrawing(Config.PadBgBrush, new Pen(Config.PadBgBrush, 1), new RectangleGeometry(Logic.Model.Pad.Area));
            FormattedText formattedText = new FormattedText(Logic.Model.Errors.ToString(), CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Arial"), 20, Brushes.Black);
            FormattedText pointsText = new FormattedText(Logic.Model.Points.ToString(), CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Arial"), 20, Brushes.Black);
            GeometryDrawing points = new GeometryDrawing(null, new Pen(Brushes.Red, 1), pointsText.BuildGeometry(new Point(Config.Width - 50, 5)));
            GeometryDrawing text = new GeometryDrawing(null, new Pen(Brushes.Red, 1), formattedText.BuildGeometry(new Point(5, 5)));
            dg.Children.Add(background);
            dg.Children.Add(ball);
            dg.Children.Add(pad);
            dg.Children.Add(text);
            dg.Children.Add(points);

            foreach (var star in Logic.Model.Stars)
            {
                GeometryDrawing sGeo = new GeometryDrawing(Config.BallBgBrush, new Pen(Config.BallLineBrush, 1), star.GetGeometry());
                dg.Children.Add(sGeo);
            }
            foreach (var enemy in Logic.Model.Enemies)
            {
                GeometryDrawing eGeo = new GeometryDrawing(Config.EnemyBgBrush, new Pen(Config.EnemyLineBrush, 1), new RectangleGeometry(enemy.Area));
                dg.Children.Add(eGeo);
            }

            ctx.DrawDrawing(dg);
        }
    }
}
