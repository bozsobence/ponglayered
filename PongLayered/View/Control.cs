﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Model;
using Logic;
using System.Windows.Threading;
using System.Windows.Input;
using System.Windows.Media;

namespace View
{
    class Control : FrameworkElement
    {
        //IGameModel model;
        ILogic logic;
        Renderer renderer;
        DispatcherTimer tickTimer;
        public Control()
        {
            Loaded += Control_Loaded;
        }

        private void Control_Loaded(object sender, RoutedEventArgs e)
        {
            //model = new GameModel();
            logic = new BusinessLogic();
            renderer = new Renderer(logic);
            Window win = Window.GetWindow(this);
            if (win != null)
            {
                tickTimer = new DispatcherTimer();
                tickTimer.Interval = TimeSpan.FromMilliseconds(40);
                tickTimer.Tick += TickTimer_Tick;
                tickTimer.Start();

                win.KeyDown += Win_KeyDown;
                MouseLeftButtonDown += Control_MouseLeftButtonDown;

            }
            logic.RefreshScreen += (obj, args) => InvalidateVisual();
            InvalidateVisual();
        }

        private void Control_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            logic.JumpPad(e.GetPosition(this).X);
        }

        private void Win_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            switch (e.Key)
            {

                case Key.Left:
                    logic.MovePad(Direction.Left);
                    break;
                case Key.Right:
                    logic.MovePad(Direction.Right);
                    break;
                case Key.Space:
                    logic.AddStar();
                    break;
            }
        }

        private void TickTimer_Tick(object sender, EventArgs e)
        {
            logic.MoveBall();
            logic.MoveStars();
            logic.MoveEnemies();
        }
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (renderer != null)
            {
                renderer.DrawThings(drawingContext);
            }
        }
    }
}
