﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Model
{
    public static class Config
    {
        public static Brush BorderBrush = Brushes.DarkGray;
        public static Brush BackgroundBrush = Brushes.Cyan;

        public static Brush BallBgBrush = Brushes.Yellow;

        public static Brush BallLineBrush = Brushes.Red;

        public static Brush PadBgBrush = Brushes.Brown;

        public static Brush PadLineBrush = Brushes.Black;

        public static Brush EnemyBgBrush = Brushes.Blue;
        public static Brush EnemyLineBrush = Brushes.Yellow;

        public static Random rnd = new Random();

        public static double Width = 2000;
        public static double Height = 700;
        public static int BorderSize = 4;
        public static int BallSize = 20;
        public static int PadWidth = 100;
        public static int PadHeight = 20;

        public static int CountOfEnemies = 30;
        public static int EnemySize = 20;


    }
}
