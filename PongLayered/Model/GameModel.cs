﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class GameModel : IGameModel
    {
        public int Errors { get; set; }
        public int Points { get; set; }
        public Pad Pad { get; set; }
        public Ball Ball { get; set; }
        public List<Star> Stars { get; set; }
        public List<Enemy> Enemies { get; set; }
        public GameModel()
        {
            Ball = new Ball(Config.Width / 2, Config.Height / 2, Config.BallSize, Config.BallSize);
            Pad = new Pad(Config.Width / 2, Config.Height - Config.PadHeight, Config.PadWidth, Config.PadHeight);
            Stars = new List<Star>();
            Enemies = new List<Enemy>();
        }
    }
}
