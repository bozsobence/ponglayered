﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Pad : Shape
    {
        public Pad(double x, double y, double w, double h) : base(x, y, w, h)
        {
        }
    }
}
