﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public interface IGameModel
    {
        int Points { get; set; }
        int Errors { get; set; }
        Pad Pad { get; set; }
        Ball Ball { get; set; }
        List<Star> Stars { get; set; }
        List<Enemy> Enemies { get; set; }
        
    }
}
