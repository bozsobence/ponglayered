﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Model
{
    public class Star : Shape
    {
        double n;
        double r;
        public Star(double x, double y, double r, double n) : base(x, y, r, n)
        {
            this.n = n;
            this.r = r;
        }

        public Geometry GetGeometry()
        {
            List<Point> points = new List<Point>();
            for (int i = 0; i < n; i++)
            {
                double angle = i * 2 * Math.PI / n;
                Point p = new Point(r * Math.Cos(angle), r * Math.Sin(angle));
                if (i % 2 == 1)
                {
                    p.X *= 0.2;
                    p.Y *= 0.2;
                }
                p.X += r + Area.X;
                p.Y += r + Area.Y;
                points.Add(p);
            }
            StreamGeometry sGeometry = new StreamGeometry();
            using (StreamGeometryContext gContext = sGeometry.Open())
            {
                gContext.BeginFigure(points[0], true, true);
                gContext.PolyLineTo(points, true, true);
            }
            return sGeometry;
        }
    }
}
