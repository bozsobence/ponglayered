﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Model
{
    public class Shape
    {
        Rect area;
        public Rect Area
        {
            get { return area; }
        }
        public int Dx { get; set; }
        public int Dy { get; set; }
        public Shape(double x, double y, double w, double h) // x,y -> top left, 
        {
            area = new Rect(x, y, w, h);
            Dx = 5;
            Dy = 5;
        }
        public void ChangeX(double diff)
        {
            area.X += diff;
        }
        public void ChangeY(double diff)
        {
            area.Y += diff;
        }
        public void SetXY(double x, double y)
        {
            area.X = x;
            area.Y = y;
        }

        public void MoveRandom()
        {
            int rand = Config.rnd.Next(101);
            int newDx = 0;
            int newDy = 0;
            if (rand > 70)
            {
                newDy = -Dy;
            }
            else
            {
                newDy = Dy;
            }

            if (area.X + Dx >= Config.Width)
            {
                newDx = -Dx;
            }
            if (area.X + Dx <= 0)
            {
                newDx = Dx;
            }
            if (area.Y + newDy >= Config.Height)
            {
                newDy = -Dy;
            }
            if (area.Y + newDy <= 0)
            {
                newDy = Dy;
            }
            area.X += newDx;
            area.Y += newDy;
        }
    }
}
